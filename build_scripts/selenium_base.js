var fs = require('fs');
var crop = require('png-crop').crop;
var webdriver = require('selenium-webdriver');
var chrome = require('selenium-webdriver/chrome');
var chromePath = require('chromedriver').path;

var screenshots = './';


function saveScreenshot(data, filename, coords) {
  var out = screenshots + filename;
  fs.writeFileSync(out, data, 'base64');
  crop(out, out, coords, function(err) {
    if (err) {
      throw err;
    }
    console.log('saved ' + out);
  });
}


module.exports = {

  driver: function(name) {
    var d;

    switch (name) {
      case 'firefox': {
        d = new webdriver.Builder().forBrowser('firefox').build();
        break;
      }

      case 'chrome':
      default: {
        var service = new chrome.ServiceBuilder(chromePath).build();
        chrome.setDefaultService(service);
        var d = new webdriver.Builder().withCapabilities(
          webdriver.Capabilities.chrome()).build();
      }
    }

    return d;
  },

  pixelMultiplier: function(name, pixelRatio) {
    var r;

    switch (name) {
      case 'chrome': {
        r = pixelRatio;
        break;
      }

      case 'firefox':
      default: {
        r = 1;
        break;
      }
    }

    return r;
  },

  capture: function(driver, filename, coords) {
    driver.takeScreenshot().then(function(data) {
      saveScreenshot(data, filename, coords);
    });
  }

};
