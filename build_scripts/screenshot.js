var argv = require('yargs')
  .usage('Usage: $0 -u [url] [options]')
  .default({width: 1000, height: 800, selector: 'body', o: 'output.png',
            driver: 'chrome'})
  .demand(['u', 'o'])
  .example('$0 -u http://deglet.xyz\n -o signup_form.png\n --selector ".site-content .loginarea"',
           'Take a screenshot of the signup form at deglet.xyz')
  .argv;
var base = require('./selenium_base');
var output = typeof argv.o === 'string' ? argv.o : 'output.png';
var driverName = typeof argv.driver === 'string' ? argv.driver : 'chrome';
var driver = base.driver(driverName);
var pixelRatio = 1;


function main(element) {
  element.getSize().then(function(size) {
    element.getLocation().then(function(location) {
      var multiplier = base.pixelMultiplier(driverName, pixelRatio);
      var coords = {
        top: location.y * multiplier,
        left: location.x * multiplier,
        width: size.width * multiplier,
        height: size.height * multiplier
      };
      base.capture(driver, output, coords);
    });
  });
}


driver.manage().window().setSize(argv.width, argv.height);

driver.get(argv.u);

driver.executeScript(function() {
  return window.devicePixelRatio;
}).then(function(result) {
  pixelRatio = result;
});

driver.findElement({css: argv.selector}).then(main).thenFinally(function() {
  driver.quit();
});
