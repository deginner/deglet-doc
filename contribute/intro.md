---
layout: default
title: Contributing
weight: 1
---

## Contributing to

### Existing transalations

In order to help with translations, visit the
[Deglet project on Crowdin](https://crowdin.com/project/deglet)
to make your contributions.

The text to be translated uses the ICU message syntax, in case you're
new to it please check <http://formatjs.io/guides/message-syntax/>
for a quick introduction and examples.


### Issues, this documentation, and others

* For issues with Deglet, please report them at
  <https://bitbucket.org/deginner/deglet/issues> in case
  the problem hasn't been reported before.
* Improve this documentation by submitting a pull request at
  <https://bitbucket.org/deginner/deglet-doc/pull-requests/>.

Other ways to contribute to Deglet are described under
[Advanced contributions]({{ "/contribute" | prepend: site.baseurl }}).
