---
layout: index
title: Index
---


## Deglet

Deglet is a Bitcoin wallet written in JavaScript. Its interface is written
using [React](http://facebook.github.io/react/),
[Flux](https://facebook.github.io/flux/),
[React Router](http://rackt.github.io/react-router/),
[React Intl](http://formatjs.io/react/), and a couple of
smaller libraries. The Bitcoin pieces are mostly from Bitcore and related
packages, forked as necessary to match the features Deglet is after.
Its source code and issue tracker are available at
<https://bitbucket.org/deginner/deglet>, and a testnet instance is
present at [deglet.xyz](http://deglet.xyz).

Welcome to the developer documentation! Follow one of the pages listed
below to read more.
